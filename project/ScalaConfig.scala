import sbt.Def

/**
  * Configures Scala for this project.
  */
object ScalaConfig {
  val CompilerOptions = Def.setting(Seq(
    "-deprecation",
    "-encoding", "UTF-8",
    "-feature","-unchecked",
    "-language:postfixOps", "-language:implicitConversions",
    "-Ywarn-dead-code",
    "-Xlint"
  ))

  /*
     We use the Typelevel compiler to fix SI-7046.
     Now we can share abstract classes with case class implementations between client and server.
  */
  val organization = "org.typelevel"

  val version = "2.11.8"
}