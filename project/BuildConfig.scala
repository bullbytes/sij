/**
  * Configuration for this project.
  */
object BuildConfig {
  val appVersion = "0.0.8"

  /**
    * SkeletonPage.scala uses this name via the BuildInfo object.
    */
  val appName = "sij"
}