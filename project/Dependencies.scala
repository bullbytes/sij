import org.scalajs.sbtplugin.ScalaJSPlugin.autoImport._
import sbt._

/**
  * Lists the dependencies for this project.
  */
object Dependencies {

  // We need to use the same (old) version of uPickle as CouchDB-Scala, otherwise we get a NoSuchMethodException when
  // creating CouchDB views. CouchDB-Scala upgrades its uPickle version in 0.8.0
  val uPickleVersion = "0.3.9"

  object client {
    // JavaScript libraries the client uses
    val jsDependencies = Def.setting(Seq(
      "org.webjars" % "jquery" % "3.1.1" / "3.1.1/jquery.min.js"
    ))

    // The triple % gets the library in two versions: One for running on the JVM and one for running on a JavaScript engine like V8
    val scalaJsDependencies = Def.setting(Seq(
      // Used to produce HTML with Scala: https://github.com/lihaoyi/scalatags
      "com.lihaoyi" %%% "scalatags" % "0.6.0",

      // Lets us create CSS to style our websites with Scala: https://github.com/japgolly/scalacss
      "com.github.japgolly.scalacss" %%% "ext-scalatags" % "0.5.0",

      // Serializes data between client and server: https://github.com/lihaoyi/upickle-pprint
      "com.lihaoyi" %%% "upickle" % uPickleVersion,

      // Type-safe API calls from the client to the server: https://github.com/lihaoyi/autowire
      "com.lihaoyi" %%% "autowire" % "0.2.5",

      // A type facade for jQuery so we can use the JavaScript library in a type-safe manner
      "be.doeraene" %%% "scalajs-jquery" % "0.9.0",

      // Logging: https://github.com/jokade/slogging
      "biz.enef" %%% "slogging" % "0.5.0",

      // A (partial) java.time implementation for JavaScript: https://github.com/scala-js/scala-js-java-time
      "org.scala-js" %%% "scalajs-java-time" % "0.2.0"
      // Alternative to scalajs-java-time: https://github.com/soc/scala-java-time
     // "io.github.soc" %%% "scala-java-time" % "2.0.0-M1"

    ))
  }

  val server = {
    Def.setting(Seq(
      // Our HTTP server: http://doc.akka.io/docs/akka-http/current/index.html
      "com.typesafe.akka" %% "akka-http" % "10.0.4",

      // Needed so the server can create an HTML page that it sends to the client
      "com.lihaoyi" %% "scalatags" % "0.6.0",

      // Used for sending data in AJAX calls between client and server. Includes serialization and deserialization
      "com.lihaoyi" %% "upickle" % uPickleVersion,
      "com.lihaoyi" %% "autowire" % "0.2.5",

      // Our database: https://github.com/beloglazov/couchdb-scala
      "com.ibm" %% "couchdb-scala" % "0.7.2",

      // Logging facade for Scala and Scala.js: https://github.com/jokade/slogging
      "biz.enef" %% "slogging-slf4j" % "0.5.0",
      // Logger: http://logback.qos.ch/
      "ch.qos.logback" % "logback-classic" % "1.1.7",
      // Needed for reading the logback.groovy configuration file
      "org.codehaus.groovy" % "groovy-all" % "2.4.7",

      // Testing: http://www.scalatest.org
      "org.scalactic" %% "scalactic" % "3.0.1",
      "org.scalatest" %% "scalatest" % "3.0.1" % "test"
    ))
  }
}