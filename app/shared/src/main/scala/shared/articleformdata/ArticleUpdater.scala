package shared.articleformdata

import shared.reducedarticleview.ReducedViewOfArticle

/**
  * Creates copies of an [[ReducedViewOfArticle]] with updated fields.
  */
object ArticleUpdater {

  def articleWithNewArticleName(oldArticle: ReducedViewOfArticle, newArticleName: String): ReducedViewOfArticle = {
    oldArticle.copy(articleName = newArticleName)
  }

  def articleWithNewDescription(oldArticle: ReducedViewOfArticle, newDescription: String): ReducedViewOfArticle = {
    oldArticle.copy(articleDescription = newDescription)
  }

  def articleWithNewArticleNrInCatalog(oldArticle: ReducedViewOfArticle, newArticleNr: String): ReducedViewOfArticle = {
    val oldCatalogData = oldArticle.catalogData
    val newCatalogData = oldCatalogData.copy(articleNrInCatalog = newArticleNr)
    oldArticle.copy(catalogData = newCatalogData)
  }

  def articleWithNewArticleNrUsedByMerchantOrSupplier(oldArticle: ReducedViewOfArticle, newArticleNr: String): ReducedViewOfArticle = {
    oldArticle.copy(articleNrAssignedByPartner = newArticleNr)
  }

  def withNewPositionInCatalog(oldArticle: ReducedViewOfArticle, newPosition: Int): ReducedViewOfArticle = {
    val oldCatalogData = oldArticle.catalogData
    val newCatalogData = oldCatalogData.copy(positionInCatalog = newPosition)
    oldArticle.copy(catalogData = newCatalogData)
  }

  def articleWithNewRemarksOnCatalogCreation(oldArticle: ReducedViewOfArticle, newRemarks: String): ReducedViewOfArticle = {
    val oldCatalogData = oldArticle.catalogData
    val newCatalogData = oldCatalogData.copy(remarksOnCatalogCreation = newRemarks)
    oldArticle.copy(catalogData = newCatalogData)
  }

  def withNewPageInCatalog(oldArticle: ReducedViewOfArticle, newPage: Int): ReducedViewOfArticle = {
    val oldCatalogData = oldArticle.catalogData
    val newCatalogData = oldCatalogData.copy(pageInCatalog = newPage)
    oldArticle.copy(catalogData = newCatalogData)
  }


  def articleWithNewSubcategory(oldArticle: ReducedViewOfArticle, newSubcategory: CatalogSubCategory): ReducedViewOfArticle = {
    val oldCatalogData = oldArticle.catalogData
    val newCatalogData = oldCatalogData.copy(subCategory = newSubcategory)
    oldArticle.copy(catalogData = newCatalogData)
  }

  /**
    * Creates a new [[ReducedViewOfArticle]] with a `newCategory` from the `oldArticle`.
    */
  def articleWithNewCategory(oldArticle: ReducedViewOfArticle, newCategory: CatalogCategory): ReducedViewOfArticle = {
    val oldCatalogData = oldArticle.catalogData
    val newCatalogData = oldCatalogData.copy(category = newCategory)
    oldArticle.copy(catalogData = newCatalogData)
  }
}

//object ArticleUpdater {

//  def articleWithNewArticleName(oldArticle: ArticleWithMetaInfo, newArticleName: String): ArticleWithMetaInfo = {
//    val oldArticleData = oldArticle.articleFormData.articleData
//    val newArticleData = oldArticleData.copy(name = newArticleName)
//    withNewArticleData(oldArticle, newArticleData)
//  }
//
//  def articleWithNewDescription(oldArticle: ArticleWithMetaInfo, newDescription: String): ArticleWithMetaInfo = {
//
//    val oldArticleData = oldArticle.articleFormData.articleData
//    val newArticleData = oldArticleData.copy(description = newDescription)
//    withNewArticleData(oldArticle, newArticleData)
//  }
//
//  def articleWithNewArticleNrInCatalog(oldArticle: ArticleWithMetaInfo, newArticleNr: String): ArticleWithMetaInfo = {
//
//    val oldCatalogData = oldArticle.articleFormData.catalogData
//    val newCatalogData = oldCatalogData.copy(articleNrInCatalog = newArticleNr)
//
//    withNewCatalogData(oldArticle, newCatalogData)
//  }
//
//  def articleWithNewArticleNrUsedByMerchantOrSupplier(oldArticle: ArticleWithMetaInfo, newArticleNr: String): ArticleWithMetaInfo = {
//
//    val oldArticleData = oldArticle.articleFormData.articleData
//    val newArticleData = oldArticleData.copy(articleNumberAssignedByMerchantOrSupplier = newArticleNr)
//    withNewArticleData(oldArticle, newArticleData)
//  }
//
//
//  def withNewPositionInCatalog(oldArticle: ArticleWithMetaInfo, newPosition: Int): ArticleWithMetaInfo = {
//    val oldCatalogData = oldArticle.articleFormData.catalogData
//    val newCatalogData = oldCatalogData.copy(positionInCatalog = newPosition)
//
//    withNewCatalogData(oldArticle, newCatalogData)
//  }
//
//  def articleWithNewRemarksOnCatalogCreation(oldArticle: ArticleWithMetaInfo, newRemarks: String) ={
//
//  val oldCatalogData = oldArticle.articleFormData.catalogData
//  val newCatalogData = oldCatalogData.copy(remarksOnCatalogCreation = newRemarks)
//
//  withNewCatalogData(oldArticle, newCatalogData)
//}
//
//  def withNewPageInCatalog(oldArticle: ArticleWithMetaInfo, newPage: Int): ArticleWithMetaInfo = {
//    val oldCatalogData = oldArticle.articleFormData.catalogData
//    val newCatalogData = oldCatalogData.copy(pageInCatalog = newPage)
//
//    withNewCatalogData(oldArticle, newCatalogData)
//  }
//
//  private def withNewCatalogData(oldArticle: ArticleWithMetaInfo, newCatalogData: CatalogData): ArticleWithMetaInfo = {
//    val newFormData = oldArticle.articleFormData.copy(catalogData = newCatalogData)
//
//    oldArticle.copy(articleFormData = newFormData)
//  }
//
//  private def withNewArticleData(oldArticle: ArticleWithMetaInfo, newArticleData: PromotionalArticle): ArticleWithMetaInfo = {
//    val newFormData = oldArticle.articleFormData.copy(articleData = newArticleData)
//    oldArticle.copy(articleFormData = newFormData)
//  }
//
//  def articleWithNewSubcategory(oldArticle: ArticleWithMetaInfo, newSubcategory: CatalogSubCategory): ArticleWithMetaInfo = {
//
//    val oldCatalogData = oldArticle.articleFormData.catalogData
//    val newCatalogData = oldCatalogData.copy(subCategory = newSubcategory)
//
//    withNewCatalogData(oldArticle, newCatalogData)
//
//  }
//
//  /**
//    * Creates a new [[ArticleWithMetaInfo]] with a `newCategory` from the `oldArticle`.
//    */
//  def articleWithNewCategory(oldArticle: ArticleWithMetaInfo, newCategory: CatalogCategory): ArticleWithMetaInfo = {
//
//    val oldCatalogData = oldArticle.articleFormData.catalogData
//    val newCatalogData = oldCatalogData.copy(category = newCategory)
//
//    withNewCatalogData(oldArticle, newCatalogData)
//
//  }
//}