package shared.articleformdata

/**
  * Information about the merchant-specific files which the user can include in the [[ArticleForm]].
  * <p>
  * Created by Matthias Braun on 10/31/2016.
  */
object MerchantSpecificFiles {

  object Descriptions {
    val cover = "catalog cover of merchant"
    val customPages = "custom pages of merchant"
  }

}
