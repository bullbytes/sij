package shared.articleformdata


import shared.articleformdata.brandings.StandardArticleBrandings
import shared.pages.HtmlId
import ArticleFormConstants.ImageUrlConstants._

import scala.collection.immutable.IndexedSeq

/**
  * IDs for HTML elements in the [[ArticleForm]].
  * <p>
  * Created by Matthias Braun on 10/13/2016.
  */
object ArticleFormIds {

  val articleForm = HtmlId("article-form")

  val fieldsWithNewDataAreHighlighted = HtmlId("fields-with-new-data-are-highlighted")

  /**
    * HTML IDs for data about merchants or suppliers of promotion articles.
    */
  object MerchantAndSupplierIds {

    // The ID of the element containing merchant or supplier data
    val data = HtmlId("merchant-and-supplier-data")

    val name = HtmlId("merchant-and-supplier-name")
  }

  /**
    * The HTML IDs pertaining to a promotion article.
    */
  object ArticleIds {

    /**
      * The ID of the element that lets the user enter article data.
      */
    val data = HtmlId("article-data-element")

    val name = HtmlId("article-name")
    val number = HtmlId("article-number")
    val description = HtmlId("article-description")
    val size = HtmlId("article-dimension-and-size")
    val colorAndShape = HtmlId("article-color-and-shape")


    /**
      * [[HtmlId]]s pertaining to the minimum order quantity of a promotional article.
      */
    object MinimumOrderQuantityIds {
      /**
        * The [[HtmlId]]s for all the fields where the user can enter the minimum order quantity for their product
        * and the condition that must be true for that minimum order quantity to be in effect
        * (e.g., "article must be branded").
        *
        * The first element in this sequence of pairs is always the ID of the condition field, the second element is
        * always the ID of the quantity field.
        */
      val conditionAndQuantityIds: IndexedSeq[(HtmlId, HtmlId)] = for {
        nrOfMinimumOrderQuantity <- 1 to ArticleFormConstants.NrOfMinimumOrderQuantities
        condition = conditionId(nrOfMinimumOrderQuantity)
        quantity = quantityId(nrOfMinimumOrderQuantity)
      } yield (condition, quantity)

      // The first element of the pair is the condition ID
      val conditionIds: IndexedSeq[HtmlId] = conditionAndQuantityIds.map(_._1)
      val quantityIds: IndexedSeq[HtmlId] = conditionAndQuantityIds.map(_._2)

      def quantityId(nrOfMinimumOrderQuantity: Int) = HtmlId(s"minimum-order-quantity-$nrOfMinimumOrderQuantity")

      def conditionId(nrOfMinimumOrderQuantity: Int) = HtmlId(s"condition-for-minimum-order-quantity-$nrOfMinimumOrderQuantity")
    }

  }

  object UnbrandedArticleIds {

    val unbrandedArticle = HtmlId("unbranded-article")
    val suggestedPrice = HtmlId("suggested-price")

    object Scales {
      /**
        * @return the [[HtmlId]]s for the input fields that contain how many unbranded articles someone has to buy to be
        *         in the particular price scale
        */
      def articleQuantityIds: IndexedSeq[HtmlId] = BrandingIds.Scales.articleQuantityIdsFor(unbrandedArticle)

      /**
        * @return the [[HtmlId]]s for the input fields that contain the price of an unbranded article for a scale
        */
      def priceIds: Seq[HtmlId] = BrandingIds.Scales.priceIdsFor(unbrandedArticle)
    }
  }

  /**
    * IDs for HTML elements that let the user enter data about a branding (finishing) of a promotional article.
    */
  object BrandingIds {
    def userDefinedBrandings: IndexedSeq[HtmlId] = for {
      userDefinedBrandingNr <- 1 to ArticleFormConstants.NrOfUserDefinedBrandings
      userDefinedBrandingId = userDefinedBranding(userDefinedBrandingNr)
    } yield userDefinedBrandingId


    def nameOfUserDefinedBranding(nrOfUserDefinedBranding: Int): HtmlId =
    nameOfUserDefinedBranding(userDefinedBranding(nrOfUserDefinedBranding))

    def userDefinedBranding(nrOfUserDefinedBranding: Int) = HtmlId(s"user-defined-branding-$nrOfUserDefinedBranding")

    def nameOfUserDefinedBranding(brandingId: HtmlId): HtmlId = HtmlId(s"name-of-$brandingId")

    def initialPrintingCosts(typeOfBranding: HtmlId) = HtmlId(s"initial-printing-costs-for-$typeOfBranding")

    def filmCosts(typeOfBranding: HtmlId) = HtmlId(s"film-costs-for-$typeOfBranding")

    /**
      * Printing with one color.
      */
    val print1c = HtmlId("print-1c")
    val printAdditionalColor = HtmlId("print-additional-color")
    val print2c = HtmlId("print-2c")
    val print3c = HtmlId("print-3c")
    val print4c = HtmlId("print-4c")
    val digitalPrint4c = HtmlId("digital-print-4c")

    val engraving = HtmlId("engraving")
    val etching = HtmlId("etching")
    val stitching = HtmlId("stitching")

    /**
      * The form presents the user with these kinds of brandings. Additionally, the user can define custom brandings.
      */
    val standard: Seq[HtmlId] = StandardArticleBrandings.ids

    val positionAndDimension = HtmlId("position-and-dimension-of-branding")

    object Scales {

      def quantityAndPriceIds(brandingId: HtmlId): IndexedSeq[(HtmlId, HtmlId)] = for {
        scaleNr <- 1 to ArticleFormConstants.NrOfScales
        quantityId = quantityForScale(scaleNr, brandingId)
        priceId = priceOfArticleForScale(scaleNr, brandingId)
      } yield (quantityId, priceId)


      /**
        * @return the [[HtmlId]]s for the input fields that contain how many articles someone has to buy to be
        *         in the particular price scale
        */
      def articleQuantityIdsFor(branding: HtmlId): IndexedSeq[HtmlId] = quantityAndPriceIds(branding).map(_._1)

      /**
        * @return the [[HtmlId]]s for the input fields that contain the price of an unbranded article for a scale
        */
      def priceIdsFor(branding: HtmlId): Seq[HtmlId] = quantityAndPriceIds(branding).map(_._2)

      def quantityForScale(scaleNr: Int, tableId: HtmlId) = HtmlId(s"nr-of-articles-of-scale-$scaleNr-for-${tableId.value}")

      def priceOfArticleForScale(scaleNr: Int, tableId: HtmlId) = HtmlId(s"price-of-${tableId.value}-of-scale-$scaleNr")

    }

  }

  /**
    * IDs for HTML elements specific to merchants of promotion articles
    */
  object MerchantIds {


    /**
      * The element where merchants can upload their finished material for the catalog as PDF files.
      */
    val merchantSpecificFiles = HtmlId("merchant-specific-print-data")

    val coverUrlInput = HtmlId("cover-url-input")

    val customPagesUrlInput = HtmlId("custom-pages-url-input")
  }

  /**
    * [[HtmlId]]s pertaining to the upload buttons of the [[ArticleForm]] where the user can upload files such as
    * images of the promotion article, logos, or finished pages of the catalog as a PDF.
    */
  object FileUploadIds {

    object ImagePreviewIds {
      val certificateGallery = HtmlId("certificates-preview")


      val brandImageDiv = HtmlId("brand-image-preview-and-remove-button")
      val brandImage = HtmlId("brand-image-preview")

      val colorSamplesGallery = HtmlId("gallery-for-color-samples")

      val additionalImagesGallery = HtmlId("gallery-for-additional-article-images")
      val additionalLogosGallery = HtmlId("gallery-for-additional-logos")

      val mainImageDiv = HtmlId("main-image-preview-and-remove-button")
      val mainImage = HtmlId("main-image-preview")
    }
  }

  object ImageUrlIds {

    val mainArticleImage = HtmlId("main-article-image-url")

    def additionalArticleImage(imageNr: Int) = HtmlId(s"additional-image-$imageNr-for-article-url")

    val additionalArticleImages: IndexedSeq[HtmlId] = (1 to NrOfAdditionalArticleImageUrls).map(additionalArticleImage)

    def colorSample(imageNr: Int) = HtmlId(s"color-sample-$imageNr-url")

    val colorSamples: IndexedSeq[HtmlId] = (1 to NrOfColorSampleUrls).map(colorSample)

    val brandImage = HtmlId("brand-image-url")

    def additionalLogo(imageNr: Int) = HtmlId(s"additional-logo-$imageNr-url")

    val additionalLogos: IndexedSeq[HtmlId] = (1 to NrOfAdditionalLogoUrls).map(additionalLogo)

    def certificate(imageNr: Int) = HtmlId(s"certificate-$imageNr-url")

    val certificates: IndexedSeq[HtmlId] = (1 to NrOfCertificateUrls).map(certificate)

  }

  val remarksOnCatalogCreation = HtmlId("remarks-for-catalog-creation")
}
