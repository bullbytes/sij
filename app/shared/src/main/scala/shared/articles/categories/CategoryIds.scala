package shared.articles.categories

/**
  * The ID of an [[shared.articles.categories.ArticleCategories.ArticleCategory]]
  * <p>
  * Created by Matthias Braun on 12/12/2016.
  */
object CategoryIds {

  case class CategoryId(value: String) extends AnyVal {
    override def toString: String = value
  }

  val office = CategoryId("office-id")

  val usbAndPowerbanksAndAccessories = CategoryId("usb-and-powerbanks-and-accessories-id")

  val technology = CategoryId("technology-id")

  val bags = CategoryId("bags-id")

  val food = CategoryId("food-id")

  val utilities = CategoryId("utilities-id")

  val leisureAndGames = CategoryId("leisure-and-games-id")

  val homeAndTravel = CategoryId("home-and-travel-id")

  val textiles = CategoryId("textiles-id")

  // The category ID for articles that don't have a category yet
  val none = CategoryId("no-category-id")

}
