package shared

import shared.articleformdata.{ArticleFormData, ArticleId, ArticleWithMetaInfo, MerchantOrSupplierId}
import shared.reducedarticleview.ReducedViewOfArticle
import shared.serverresponses.ServerResponse

import scala.concurrent.Future

/**
  * Defines which AJAX requests the client can make to the server.
  * <p>
  * Created by Matthias Braun on 9/4/2016.
  */
trait SijAjaxApi {

  // It seems that with Autowire, method overloading is not possible

  def saveNewArticle(data: ArticleFormData): Future[ServerResponse]

  def saveNewVersionOfArticle(dataWithId: ArticleWithMetaInfo): Future[ServerResponse]

  def updateArticles(articlesToSave: Seq[ReducedViewOfArticle]): Future[ServerResponse]

  def getLatestAndPreviousArticle(id: ArticleId): Future[ServerResponse]

  def getReducedViewOfLatestArticles(): Future[ServerResponse]

  def getLatestArticlesOf(id: MerchantOrSupplierId): Future[ServerResponse]

  def deleteArticle(articleId: ArticleId): Future[ServerResponse]

}
