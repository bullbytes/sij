package shared.pages

/**
  * A string that appears in the graphical user interface.
  *
  * @param value the content of the [[GuiString]]
  */
case class GuiString(value: String) extends AnyVal {
  override def toString: String = value
}

