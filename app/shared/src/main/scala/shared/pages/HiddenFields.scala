package shared.pages

/**
  * These hidden input fields are used to send data from the server to the client.
  * <p>
  * Created by Matthias Braun on 10/24/2016.
  */
object HiddenFields {


  /**
    * The key is written to the input field's name and the value to the field's value
    */
  object HiddenFieldKeys {

    val ArticleId = "article-id"
    val ArticleName = "article-name"

    val MerchantOrSupplierId = "merchant-or-supplier-id"

    val MerchantOrSupplierRights = "merchant-or-supplier-rights"

  }

  val divWithFields = HtmlId("hidden-values")
}
