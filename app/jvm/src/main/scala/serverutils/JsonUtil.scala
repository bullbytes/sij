package serverutils

import shared.articleformdata.ArticleWithMetaInfo

/**
  * Helps reading and writing the JavaScript Object Notation
  * <p>
  * Created by Matthias Braun on 1/13/2017.
  */
object JsonUtil {
  def toJson(articles: Seq[ArticleWithMetaInfo]): String = upickle.json.write(upickle.default.writeJs(articles))
}
