package server.persistency

import com.ibm.couchdb.Res.DocOk
import server.persistency.couchdb.CouchDbConnector
import server.versioning.ArticleVersions
import shared.articleformdata.{ArticleFormData, ArticleId, ArticleWithMetaInfo, MerchantOrSupplierId}
import shared.reducedarticleview.ReducedViewOfArticle
import slogging.LazyLogging

import scalaz.concurrent.Future
import scalaz.{-\/, \/, \/-}

/**
  * A repository to save and get [[shared.articleformdata.PromotionalArticle]]s.
  * <p>
  * Created by Matthias Braun on 12/10/2016.
  */
object Articles extends LazyLogging {


  def saveFirstVersion(formData: ArticleFormData): Future[\/[Throwable, DocOk]] = dbConnector.add(formData)

  def saveNewVersion(articleWithMetaInfo: ArticleWithMetaInfo): Future[\/[Throwable, DocOk]] =
    dbConnector.add(incrementVersionAndSetCreationDate(articleWithMetaInfo))

  private def incrementVersionAndSetCreationDate(article: ArticleWithMetaInfo): ArticleWithMetaInfo = {
    val articleWithNewVersion = ArticleVersions.incrementVersion(article)
    ArticleVersions.setCreationDate(articleWithNewVersion, System.currentTimeMillis)
  }

  def updateArticles(articlesToSave: Seq[ReducedViewOfArticle]): Future[\/[Throwable, Seq[DocOk]]] = {
    // Saving the new article versions is a blocking operation since trying to save them all at once, which
    // requires fetching the big ArticleWithMetaInfo objects from the DB, causes an out of memory error when having 1GB of RAM
    Future(dbConnector.save(articlesToSave))
  }

  def delete(articleId: ArticleId): Future[\/[Throwable, Seq[DocOk]]] = dbConnector.delete(articleId)

  // Connects to our CouchDB database
  val dbConnector = CouchDbConnector(DbConfig.connectionInfo)

  def latestArticles(articles: Seq[ArticleWithMetaInfo]): Seq[ArticleWithMetaInfo] = {
    // Group the articles by their ID
    val articlesById = articles.groupBy(_.metaInfo.articleId)
    // Get the latest version of each of the articles
    articlesById.flatMap { case (articleId, articlesWithSameId) => latestVersion(articlesWithSameId) }.toSeq
  }

  private def latestVersionsOfReducedArticles(articles: Seq[ReducedViewOfArticle]): Seq[ReducedViewOfArticle] = {
    // Group the articles by their ID
    val articlesById = articles.groupBy(_.articleId)
    // Get the latest version of each of the articles
    articlesById.flatMap { case (articleId, articlesWithSameId) => latestVersionOfReducedArticles(articlesWithSameId) }.toSeq
  }


  def latestArticlesOf(id: MerchantOrSupplierId): Future[\/[Throwable, Seq[ArticleWithMetaInfo]]] =
  // Get all versions of all the articles of the merchant or supplier. Then get the latest versions of those articles
    dbConnector.getAllArticlesOf(id).map(futureArticles => futureArticles.map(latestArticles))

  def latestArticlesOf(ids: Seq[MerchantOrSupplierId]): Future[\/[Throwable, Seq[ArticleWithMetaInfo]]] = {

    // We get the articles for each merchant or supplier. Then we either concatenate all the articles or return
    // the first error that occurred in the database

    val futureEmptyListRight = Future(\/-(Nil)): Future[Throwable \/ Seq[ArticleWithMetaInfo]]
    val results = ids.map(id => latestArticlesOf(id))
    val foldedResults = results.foldLeft(futureEmptyListRight) {
      (futureEither1, futureEither2) => {
        futureEither1.flatMap(either1 => {
          futureEither2.map(either2 => {
            val eitherPair = (either1, either2)
            eitherPair match {
              case (\/-(articles1), \/-(articles2)) => \/-(articles1 ++ articles2)
              case (-\/(throwable), _) => -\/(throwable)
              case (_, -\/(throwable)) => -\/(throwable)
            }
          })
        })
      }
    }
    foldedResults
  }

  //    val result = ids.map(id => latestArticlesOf(id)).foldLeft(futureEmptyListRight) {
  //      (futureErrorOrArticles1, futureErrorOrArticles2) => {
  //        val errorOrArticlesPair= for {
  //          errorOrArticles1 <- futureErrorOrArticles1
  //          errorOrArticles2 <- futureErrorOrArticles2
  ////          articles1 <- errorOrArticles1
  ////          articles2 <- errorOrArticles2
  //        } yield (errorOrArticles1 , errorOrArticles2)
  //
  //        errorOrArticlesPair
  //      }
  //}
  //result
  //    val tasks =ids.map(id=>latestArticlesOf(id)).map(futureErrorOrArticles=>futureErrorOrArticles
  //      .map(errorOrArticle=>errorOrArticle))

  // This is the initial accumulator for the fold below
  //    val emptyListRight = \/-(Nil): Throwable \/ Seq[ArticleWithMetaInfo]
  //    ids.map(id => latestArticlesOf(id)).map(futureErrorsOrArticles => futureErrorsOrArticles
  //      .map(articles => articles.foldRight(emptyListRight) {
  //      (errorOrArticles1, errorOrArticles2) =>
  //        for (articles1 <- errorOrArticles1; articles2 <- errorOrArticles2)
  //          yield articles1 ++ articles2
  //    }))
  //}

  def reducedViewOfLatestArticles: Future[\/[Throwable, Seq[ReducedViewOfArticle]]] =
    dbConnector.getReducedViewOAllArticles
      .map(errorOrArticles => errorOrArticles.map(latestVersionsOfReducedArticles))

  private def latestVersion(articlesWithSameId: Seq[ArticleWithMetaInfo]): Option[ArticleWithMetaInfo] =
    articlesWithSameId.sortBy(_.metaInfo.versionCount).lastOption

  private def latestVersionOfReducedArticles(articlesWithSameId: Seq[ReducedViewOfArticle]): Option[ReducedViewOfArticle] =
    articlesWithSameId.sortBy(_.versionCount).lastOption

  private def previousVersion(allVersionsOfArticle: Seq[ArticleWithMetaInfo]): Option[ArticleWithMetaInfo] = {
    val nrOfVersions = allVersionsOfArticle.size
    if (nrOfVersions > 1) {
      val sortedVersionsOfArticle = allVersionsOfArticle.sortBy(_.metaInfo.versionCount)
      // Get the penultimate version of the article
      Some(sortedVersionsOfArticle(nrOfVersions - 2))
    }
    else None
  }

  def latestAndPreviousArticle(id: ArticleId): Future[\/[Throwable, Option[(ArticleWithMetaInfo, Option[ArticleWithMetaInfo])]]] = {
    val result = dbConnector.getArticleVersionsByArticleId(id).map(allVersionsOrError => allVersionsOrError.map(
      allVersionsOfArticle => latestVersion(allVersionsOfArticle)
        // Get the most current version of the article and optionally the version before that
        .map(latestArticle => (latestArticle, previousVersion(allVersionsOfArticle)
      ))))
    result
  }
}
