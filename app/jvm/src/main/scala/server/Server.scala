package server


import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import server.persistency.Articles
import server.routes.SijRouter
import server.strings.ServerLogStrings.ServerStringImplicits.serverString2String
import server.strings.{ServerLogStrings, ServerStringsForClient}
import serverutils.Tasks
import shared.articleformdata.{ArticleFormData, ArticleId, ArticleWithMetaInfo, MerchantOrSupplierId}
import shared.reducedarticleview.ReducedViewOfArticle
import shared.serverresponses._
import shared.{SijAjaxApi, _}
import slogging.LazyLogging

import scala.concurrent.Future
import scala.util.Properties
import scalaz.concurrent.{Future => FutureZ}
import scalaz.{-\/, \/-}

/**
  * The server lets clients add, change, and retrieve promotional articles.
  */
object Server extends SijAjaxApi with LazyLogging {

  // A repository for saving and retrieving promotional articles
  val articles = Articles

  /**
    * Starts the server.
    */
  def up(): Unit = {
    implicit val system = ActorSystem()
    implicit val materializer = ActorMaterializer()
    implicit val executionContext = system.dispatcher

    val port = Properties.envOrElse("PORT", "8080").toInt
    val host = "0.0.0.0"

    Http().bindAndHandle(SijRouter.route, host, port)

    logger.info(s"Server started. Host: $host:$port")
  }


  override def getLatestAndPreviousArticle(id: ArticleId): Future[ServerResponse] = {
    logger.info(ServerLogStrings.requestForLatestAndPreviousArticle(id))
    val retrievalResult = articles.latestAndPreviousArticle(id)

    val future: FutureZ[ServerResponse] = retrievalResult.map {
      case -\/(e) =>
        e match {
          // CouchDB throws this exceptions when we try to get an element by an unknown ID. There might be better ways.
          case _: NoSuchMethodError | _: NoClassDefFoundError =>
            ServerError(ServerStringsForClient.noArticleWithId(id))
          case other => ServerError(ServerStringsForClient.errorInDb(other))
        }

      case \/-(latestAndPrevVersionMaybe) => latestAndPrevVersionMaybe match {
        case Some((latestArticle: ArticleWithMetaInfo, prevVersionMaybe: Option[prevArticleMaybe])) =>
          OfferLatestArticleWithPreviousVersion(latestArticle, prevVersionMaybe)

        case other => ServerError(ServerStringsForClient.noArticleWithId(id))
      }
    }
    Tasks.toScalaFuture(future)
  }

  override def getReducedViewOfLatestArticles(): Future[ServerResponse] = {
    logger.info(ServerLogStrings.requestForAllArticles)
    val startTime = System.nanoTime()

    val future: FutureZ[ServerResponse] = articles.reducedViewOfLatestArticles.map {
      case \/-(latestArticles) =>
        logger.info(s"Offering all ${latestArticles.size} articles")
        val durationInSeconds = (System.nanoTime() - startTime).toDouble / 1000000000.0
        logger.info(s"Getting articles took $durationInSeconds seconds")
        OfferReducedViewOfArticles(latestArticles)
      case -\/(e) => ServerError(ServerStringsForClient.errorInDb(e))
    }
    Tasks.toScalaFuture(future)
  }

  override def getLatestArticlesOf(id: MerchantOrSupplierId): Future[ServerResponse] = {
    logger.info(ServerLogStrings.requestForLatestArticlesOfMerchantOrSupplier(id))
    val retrievalResult = articles.latestArticlesOf(id)

    val scalazFuture = retrievalResult.map {
      case -\/(e) =>
        logger.warn(ServerLogStrings.errorInDb(e))
        ServerError(ServerStringsForClient.errorInDb(e))
      case \/-(latestArticles) =>

        logger.info(ServerLogStrings.gotArticlesForMerchantOrSupplier(latestArticles.size, id))
        OfferArticles(latestArticles)
    }
    Tasks.toScalaFuture(scalazFuture)
  }

  override def deleteArticle(articleId: ArticleId): Future[ServerResponse] = {
    logger.info(ServerLogStrings.requestToDeleteArticle(articleId))
    val deletionResult = articles.delete(articleId)

    val scalaZFuture = deletionResult map {
      case -\/(e) =>
        logger.warn(ServerLogStrings.couldNotDeleteArticle(articleId, e))
        ServerError(ServerStringsForClient.exceptionWhileDeletingArticle(articleId, e))
      case \/-(_) =>
        logger.info(ServerLogStrings.deletedArticle(articleId))
        DeletedArticle(articleId)
    }
    Tasks.toScalaFuture(scalaZFuture)
  }

  override def saveNewArticle(formData: ArticleFormData): Future[ServerResponse] = {
    val articleName = formData.articleData.name
    if (Deadlines.deadlineHasPassedFor(formData.merchantOrSupplier)) {
      logger.info(ServerLogStrings.deadlineHasPassed(articleName))
      Future.successful(DeadlineHasPassed(ServerStringsForClient.deadlineHasPassed))
    } else {

      logger.info(ServerLogStrings.savingArticle(articleName))
      val saveResult = articles.saveFirstVersion(formData)

      val scalazFuture = saveResult.map {
        case -\/(e) => logger.warn(ServerLogStrings.couldNotSaveArticle(articleName, e))
          GotArticleButCouldNotSave(ServerStringsForClient.couldNotSaveArticle(articleName, e))

        case \/-(result) => logger.info(ServerLogStrings.savedArticle(articleName, result.id))
          GotArticleAndSavedIt(ServerStringsForClient.receivedArticle(articleName))
      }
      Tasks.toScalaFuture(scalazFuture)
    }

  }

  override def saveNewVersionOfArticle(articleWithMetaInfo: ArticleWithMetaInfo): Future[ServerResponse] = {

    val articleName = articleWithMetaInfo.articleFormData.articleData.name

    if (Deadlines.deadlineHasPassedFor(articleWithMetaInfo.articleFormData.merchantOrSupplier)) {
      logger.info(ServerLogStrings.deadlineHasPassed(articleName))
      Future.successful(DeadlineHasPassed(ServerStringsForClient.deadlineHasPassed))
    } else {
      logger.info(ServerLogStrings.gotNewVersionOfArticle(articleName))
      val saveResult = articles.saveNewVersion(articleWithMetaInfo)

      val scalazFuture = saveResult.map {
        case -\/(e) => logger.warn(ServerLogStrings.couldNotSaveNewArticleVersion(articleName, e))
          GotArticleButCouldNotSave(ServerStringsForClient.couldNotSaveArticle(articleName, e))

        case \/-(result) => logger.info(ServerLogStrings.savedNewArticleVersion(articleName, result.id))
          GotArticleAndSavedIt(ServerStringsForClient.gotNewVersionOfArticle(articleName))
      }
      Tasks.toScalaFuture(scalazFuture)
    }
  }

  override def updateArticles(articlesToSave: Seq[ReducedViewOfArticle]): Future[ServerResponse] = {

    logger.info(ServerLogStrings.gotNewVersionOfArticles(articlesToSave))
    val startTime = System.nanoTime()

    val futureSaveResults = articles.updateArticles(articlesToSave)

    val scalazFuture = futureSaveResults.map {
      case -\/(e) =>
        logger.warn(ServerLogStrings.couldNotSaveNewArticleVersions(articlesToSave, e))
        GotArticlesButCouldNotSave(ServerStringsForClient.couldNotSaveArticles(articlesToSave, e))

      case \/-(results) =>
        val durationInSeconds = (System.nanoTime() - startTime).toDouble / 1000000000.0
        logger.info(ServerLogStrings.savingArticlesTook(durationInSeconds))
        logger.info(ServerLogStrings.savedNewArticleVersions(articlesToSave))
        GotArticlesAndSavedThem(ServerStringsForClient.gotNewVersionOfArticles(articlesToSave))
    }
    Tasks.toScalaFuture(scalazFuture)
  }
}
