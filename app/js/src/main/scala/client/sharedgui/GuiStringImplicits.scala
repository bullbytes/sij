package client.sharedgui

import shared.pages.GuiString

import scalatags.JsDom.StringFrag

/**
  * Whenever a function needs a [[StringFrag]] but is given a [[GuiString]], the implicit function of this object
  * converts the argument from [[StringFrag]] to [[GuiString]]. This is convenient when creating HTML using ScalaTags.
  * To make this implicit conversion available, import this object.
  * <p>
  * Created by Matthias Braun on 1/20/2017.
  */
object GuiStringImplicits {
  implicit def guiString2StringFrag(guiString: GuiString): StringFrag = StringFrag(guiString.value)
}
