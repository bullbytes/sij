package client.sharedgui

import org.scalajs.dom.html.{HR, Span}

import scalacss.ScalatagsCss._
import scalatags.JsDom.TypedTag
import scalatags.JsDom.all._

/**
  * HTML elements shared by the pages of this application.
  * <p>
  * Created by Matthias Braun on 11/7/2016.
  */
object SharedElements {

  val horizontalSpace: TypedTag[Span] = span(SharedHtmlClasses.horizontalSpace)

  val strongSeparator: TypedTag[HR] = hr(SharedHtmlClasses.strongSeparator)
  val subtleSeparator: TypedTag[HR] = hr(SharedHtmlClasses.subtleSeparator)

}
