package client.pages.articleform

import org.scalajs.dom.html.Span
import shared.articleformdata.ArticleFormConstants
import shared.pages.GuiString

import scalatags.JsDom.TypedTag

/**
  * These strings show up for the user to read on the [[ArticleForm]].
  * <p>
  * Created by Matthias Braun on 10/12/2016.
  */
object ArticleFormGuiStrings {


  val formTitle = GuiString("VÖW Produktkatalog 2017")

  val remarksForCatalogCreation = GuiString("Anmerkungen zur Katalogerstellung")

  val fieldsMarkedWithRequiredDataCharMustBeFilledOut = GuiString(s"Felder mit ${ArticleForm.RequiredDataChar} müssen ausgefüllt werden")

  val fieldsWithNewDataAreHighlighted: TypedTag[Span] = {
    import scalacss.ScalatagsCss._
    import scalatags.JsDom.all._
    span("Felder mit neuen Daten werden ", span(ArticleFormClasses.valueChangedSincePreviousVersion)("hervorgehoben"))
  }


  object MerchantOrSupplierStrings {
    val yourCompanyData = GuiString("Ihre Firmendaten")
    val name = GuiString("Firmenname")
    val unknownMerchantOrSupplier = GuiString("Unbekannter Händler oder Lieferant")
  }

  // Strings pertaining to a promotion article
  object ArticleStrings {

    val submit = GuiString("Abschicken")

    val cancel = GuiString("Abbrechen")

    val suggestedPriceInfo = GuiString("Wenn keine Staffeln vorhanden sind, geben Sie bitte einen Richtpreis an:")

    val infoAboutPrice = GuiString("Bitte tragen Sie die Preise als Endkundenpreise lt. österreichischen Katalogen \"Frei Haus\" ein:")

    val brandings = GuiString("Veredelungen")

    val suggestedPrice = GuiString("Richtpreis")

    val priceOfArticleOfScale = GuiString("Preis")

    def scale(scaleNr: Int) = GuiString(s"Staffel $scaleNr")

    val yourArticle = GuiString("Ihr Artikel")
    val number = GuiString("Artikelnummer")
    val name = GuiString("Artikelbezeichnung")
    val description = GuiString("Artikelbeschreibung")
    val dimensionAndSize = GuiString("Maße/Größe(n)")
    val colorAndShape = GuiString("Farben/Formen")
    val descriptionPlaceholder = GuiString(s"Ihr Artikel, beschrieben in maximal ${ArticleFormConstants.ArticleDescriptionMaxLength} Zeichen")
    val dimensionAndSizePlaceholder = GuiString("zB.: Höhe 99 mm, Ø 99 mm")

    /**
      * A buyer has to order this many articles to get the price for a specific price scale
      */
    val quantityForScale = GuiString("Stückzahl")

    val scalesAndPricesForUnbrandedArticles = GuiString("Staffeln und Preise")


    /**
      * [[GuiString]]s pertaining to the minimum order quantity of a promotional article.
      */
    object MinimumOrderQuantity {

      val condition = GuiString("Voraussetzung")
      val conditionPlaceholder = GuiString("z.B.: veredelt, nicht veredelt")

      val quantity = GuiString("Mindestbestellmenge")
      val quantities = GuiString("Mindestbestellmengen")

      /**
        * The user can define multiple minimum order quantities for their promotional article on the form.
        * This creates the header for one such field set.
        *
        * @param nrOfMinimumOrderQuantity the number of the field set of this minimum order quantity
        * @return the header for a field set that lets the user describe a minimum order quantity of their promotional
        *         article
        */
      def header(nrOfMinimumOrderQuantity: Int) = GuiString(s"$quantity $nrOfMinimumOrderQuantity")

    }

  }

  object ImageStrings {
    val infoAboutImageLinks = GuiString("Bitte laden Sie Ihre Bilder mit Dropbox oder Google Drive hoch."+
      " Fügen Sie danach die Links zu den Bildern hier ein:")

    val images = GuiString("Bilder")

    val mainImage = GuiString("Hauptbild des Artikels")

    def additionalArticleImage(imageNr: Int) = GuiString(s"Zusätzl. Artikelbild $imageNr")

    def colorSample(imageNr: Int) = GuiString(s"Farbpalette / Farbabbildung $imageNr")

    val brandImage = GuiString("Markenlogo")

    def additionalLogo(imageNr: Int) = GuiString(s"Weiteres Logo $imageNr (z.B. Made in ...)")

    def certificate(imageNr: Int) = GuiString(s"Zertifikat $imageNr (TÜV, Bio, etc.)")

    object UrlPlaceholders {


      val mainImage = GuiString("http://www.mein-Artikelbild.com")

      def certificateUrl(imageNr: Int) = GuiString(s"http://www.mein-Zertifikatbild-Nummer-$imageNr.com")

      val brandImage = GuiString("http://www.mein-Markenbild.com")

      def additionalLogoUrl(imageNr: Int) = GuiString(s"http://www.mein-zusätzliches-Logo-Nummer-$imageNr.com")

      def colorSampleUrl(imageNr: Int) = GuiString(s"http://www.meine-Farbpalette-Nummer-$imageNr.com")

      def additionalArticleImageUrl(imageNr: Int) = GuiString(s"http://www.meine-Farbpalette-Nummer-$imageNr.com")

      // Merchant-specific files
      val merchantCover = GuiString("http://www.mein-Umschlag.com")
      val merchantCustomPages = GuiString("http://www.meine-Katalogseiten.com")
    }


  }

  /**
    * [[GuiString]]s for the branding (finishing) of a promotion article.
    */
  object BrandingStrings {

    def userDefinedBranding(nrOfUserDefinedBranding: Int) = GuiString(s"Sonstige Veredelung $nrOfUserDefinedBranding")

    val nameOfUserDefinedBranding = GuiString("Bezeichnung")

    val initialPrintingCostsInfo = GuiString("Bitte geben Sie die gesamten Vorkosten inklusive Handlingkosten an:")
    val initialPrintingCosts = GuiString("Druckvorkosten")
    val filmCosts = GuiString("Filmkosten")

    val print1c = GuiString("Druck 1c")
    val printAdditionalColor = GuiString("Druck jede weitere Farbe")
    val print2c = GuiString("Druck 2c")
    val print3c = GuiString("Druck 3c")
    val print4c = GuiString("Druck 4c")
    val digitalPrint4c = GuiString("Digitaldruck 4c")

    val unknownBranding = GuiString("Unbekannte Veredelung")

    val engraving = GuiString("Gravur")
    val etching = GuiString("Ätzung")
    val stitching = GuiString("Stickung")

    val infoAboutBrandingPrice = GuiString("Bitte geben Sie die Kosten der Veredelung pro Stück an.")

    val positionAndDimension = GuiString("Veredelungsfläche (Position und Maße)")
    val positionAndDimensionPlaceholder = GuiString("zB.: seitlich, 35 x 35 mm")
  }

  /**
    * [[GuiString]]s pertaining to the upload buttons of the [[ArticleForm]] where the user can upload files such as
    * images of the promotion article, logos, or finished pages of the catalog as a PDF.
    */
  object FileUploadStrings {
    val certificates = GuiString("Zertifikate (TÜV, Bio, etc.) hochladen")

    val additionalLogos = GuiString("Weitere Logos (Made in ..., etc.) hochladen")

    val brandImage = GuiString("Markenlogo hochladen")

    val colorSamples = GuiString("Farbpalette/Farbabbildungen hochladen")

    val removeFile = GuiString("Entfernen")

    object ImageAltStrings {
      val default = GuiString("Das Bild kann nicht angezeigt werden. " +
        "Sie können die Datei dennoch hochladen")
    }

    val mainImage = GuiString("Ein Hauptbild des Artikels hochladen")

    val additionalImages = GuiString("Weitere Artikelbilder hochladen")
  }

  object MerchantStrings {

    val fileInfoText = GuiString("Bitte laden Sie Ihre Dateien mit Dropbox oder Google Drive hoch."+
      " Fügen Sie danach die Links zu den Bildern hier ein:")

    val printData = GuiString("VÖW-Händler fertige Druckdaten")
    val cover = GuiString("Umschlag Druck-PDF inkl. 3 mm Überfüller")
    val customPages = GuiString("16 Innenseiten Druck-PDF inkl. 3 mm Überfüller")


  }


}
