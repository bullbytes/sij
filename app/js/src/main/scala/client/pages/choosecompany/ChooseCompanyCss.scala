package client.pages.choosecompany

import client.sharedgui.SharedCss

import scalacss.Defaults._

/**
  * Provides cascading styles sheets for the [[ChooseCompany]] page.
  * <p>
  * Created by Matthias Braun on 11/2/2016.
  */
object ChooseCompanyCss extends SharedCss {

  import dsl._

  "body" - defaultBackgroundColor

  forStyle(ChooseCompanyClasses.merchantsAndSuppliers) - (
    paddingLeft(2 em),
    paddingRight(2 em),
    textAlign.center
    )


  forStyle(ChooseCompanyClasses.supplierButton) - (
    marginBottom(1 em),
    /* Text is white */
    color.white,
    borderRadius(4 px),
    textShadow := getTextShadow(xOffset = 0 px, yOffset = 2 px, blurRadius = 2 px, shadowColor = rgba(0, 0, 0, 0.3)),
    /* a light blue */
    background := rgb(51, 214, 111)
    )

  forStyle(ChooseCompanyClasses.merchantButton) - (
    marginBottom(1 em),
    /* Text is white */
    color.white,
    borderRadius(4 px),
    textShadow := getTextShadow(xOffset = 0 px, yOffset = 2 px, blurRadius = 2 px, shadowColor = rgba(0, 0, 0, 0.3)),
    /* a light blue */
    background := rgb(66, 184, 221)
    )


}
