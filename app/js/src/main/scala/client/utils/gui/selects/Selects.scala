package client.utils.gui.selects

import client.sharedgui.GuiStringImplicits.guiString2StringFrag
import org.scalajs.dom.Event
import org.scalajs.dom.html.{Option, Select}
import shared.pages.GuiString
import slogging.LazyLogging

import scalatags.JsDom.all._

/**
  * Lets the user select options from, for example, a drop-down list
  * <p>
  * Created by Matthias Braun on 12/12/2016.
  */
object Selects extends LazyLogging {
  type OnChange = (SelectsValue => Unit)

  private val dontSelectAnything = SelectsValue("do-not-select-anything")

  private def createOption(optionValue: SelectsValue, name: GuiString, shouldBeSelected: Boolean): Option = {
    val optionElem = option(value := optionValue.value)(name).render
    optionElem.selected = shouldBeSelected
    optionElem
  }

  def dropDown(options: Seq[(SelectsValue, GuiString)], onChange: OnChange): Select = {
    dropDown(options, dontSelectAnything, onChange)
  }

  def dropDown(options: Seq[(SelectsValue, GuiString)], optionToSelect: SelectsValue, onChange: OnChange): Select = {
    val dropDown = select(options.map { case (optionValue, optionName) =>
      val shouldBeSelected = optionValue == optionToSelect
      createOption(optionValue, optionName, shouldBeSelected)
    }).render

    dropDown.onchange = (_: Event) => onChange(SelectsValue(dropDown.value))
    dropDown
  }

  def dropDown(options: Seq[(SelectsValue, GuiString)]): Select = {
    dropDown(options, dontSelectAnything, (value: SelectsValue) => logger.info(s"$value was selected"))
  }
}

