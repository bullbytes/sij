package client.utils.gui.inputs

import client.sharedgui.SharedHtmlClasses
import client.utils.Keyboard
import org.scalajs.dom.Event
import org.scalajs.dom.html.{Div, Input}
import shared.pages.{GuiString, HtmlId}

import scalacss.ScalatagsCss._
import scalatags.JsDom.TypedTag
import scalatags.JsDom.all._

/**
  * The user can enter text in these HTML inputs.
  * <p>
  * Created by Matthias Braun on 12/16/2016.
  */
object TextInputs {


  private val longInputModifier = SharedHtmlClasses.toModifier(SharedHtmlClasses.longTextInputs)
  // We need a special modifier since in a table row, the one we use for regular inputs doesn't work
  private val longInputInRowsModifier = SharedHtmlClasses.toModifier(SharedHtmlClasses.longTextInputInRows)

  private val shortInputModifier = SharedHtmlClasses.toModifier(SharedHtmlClasses.shortInput)
  private val veryLongInputModifier = SharedHtmlClasses.toModifier(SharedHtmlClasses.veryLongInput)

  /**
    * Per default, hitting enter in a form submits the whole form. By setting `onkeydown`, we prevent that behavior.
    */
  private def noSubmitOnEnter = onkeydown := ((e: Event) => if (Keyboard.isEnter(e)) e.preventDefault())

  def emptyWithPlaceholder(inputId: HtmlId, placeholderText: GuiString, inputAttributes: Modifier*): Input =
    emptyWithPlaceholder(inputId, placeholderText, (_: String) => {}, inputAttributes)

  def emptyWithPlaceholder(inputId: HtmlId, placeholderText: GuiString, onChange: (String) => Unit,
                           inputAttributes: Modifier*): Input =
    withPlaceholder(inputId, placeholderText, GuiString(""), onChange, inputAttributes)

  def shortWithPlaceholder(id: HtmlId, placeholderText: GuiString, inputText: GuiString, onChange: (String) => Unit,
                           inputAttributes: Modifier*): Input =
    withPlaceholder(id, placeholderText, inputText, onChange, inputAttributes :+ shortInputModifier)


  def longWithPlaceholderInRow(inputId: HtmlId, placeholderText: GuiString, inputText: GuiString, onChange: (String) => Unit,
                          inputAttributes: Modifier*): Input =
    withPlaceholder(inputId, placeholderText, inputText, onChange, inputAttributes :+ longInputInRowsModifier)

  def longWithPlaceholder(inputId: HtmlId, placeholderText: GuiString, inputText: GuiString, onChange: (String) => Unit,
                          inputAttributes: Modifier*): Input =
    withPlaceholder(inputId, placeholderText, inputText, onChange, inputAttributes :+ longInputModifier)

  def withPlaceholder(inputId: HtmlId, placeholderText: GuiString, inputText: GuiString, onChange: (String) => Unit,
                      inputAttributes: Modifier*): Input = {

    val textInput = input(id := inputId.value, placeholder := placeholderText.value,
      value := inputText.value, noSubmitOnEnter, inputAttributes).render

    textInput.onchange = (_: Event) => onChange(textInput.value)
    textInput
  }

  def longWithLabel(labelText: GuiString, labelId: HtmlId, inputAttributes: Modifier*): TypedTag[Div] =
    longWithLabel(labelText, labelId, placeholderText = GuiString(""), inputAttributes)

  def withLabel(labelText: GuiString, labelId: HtmlId, inputText: GuiString, placeholderText: GuiString,
                inputAttributes: Modifier*): TypedTag[Div] =

    div(SharedHtmlClasses.controlGroup)(
      label(`for` := labelId.value)(labelText.value),
      input(id := labelId.value, `type` := "text", value := inputText.value, placeholder := placeholderText.value,
        noSubmitOnEnter, inputAttributes)
    )

  def longWithLabel(labelText: GuiString, labelId: HtmlId, placeholderText: GuiString,
                    inputAttributes: Modifier*): TypedTag[Div] =
    withLabel(labelText, labelId, inputText = GuiString(""), placeholderText = placeholderText,
      inputAttributes :+ longInputModifier)

  def veryLongTextInputWithLabel(labelText: GuiString, labelId: HtmlId, placeholderText: GuiString,
                                 inputAttributes: Modifier*): TypedTag[Div] =

    withLabel(labelText, labelId, inputText = GuiString(""), placeholderText = placeholderText,
      inputAttributes :+ veryLongInputModifier)


  def withLabel(labelText: GuiString, labelId: HtmlId, inputAttributes: Modifier*): TypedTag[Div] = {
    withLabel(labelText, labelId, inputText = GuiString(""), placeholderText = GuiString(""), inputAttributes)
  }

  def withLabel(labelText: GuiString, labelId: HtmlId, inputText: GuiString,
                inputAttributes: Modifier*): TypedTag[Div] = {
    withLabel(labelText, labelId, inputText = inputText, placeholderText = GuiString(""), inputAttributes)
  }
}
