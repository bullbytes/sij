package client.utils

import autowire._
import org.scalajs.dom
import shared.articleformdata.{ArticleFormData, ArticleId, ArticleWithMetaInfo, MerchantOrSupplierId}
import shared.reducedarticleview.ReducedViewOfArticle
import shared.serverresponses.ServerResponse
import shared.{SijAjaxApi, SijRestApi}

import scala.concurrent.Future
import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue

/**
  * Using Autowire, we can do AJAX calls to the server in a type-safe manner.
  * <p>
  * Created by Matthias Braun on 10/24/2016.
  */
object Ajaxer extends autowire.Client[String, upickle.default.Reader, upickle.default.Writer] {

  override def doCall(req: Request): Future[String] = {
    dom.ext.Ajax.post(
      url = "/" + SijRestApi.AjaxPath + "/" + req.path.mkString("/"),
      data = upickle.default.write(req.args)
    ).map(_.responseText)
  }

  // There must be a Reader[Result] implicitly available
  def read[Result: upickle.default.Reader](string: String): Result = upickle.default.read[Result](string)

  // There must be a Writer[Result] implicitly available
  def write[Result: upickle.default.Writer](result: Result): String = upickle.default.write(result)

  def getLatestAndPreviousArticle(id: ArticleId): Future[ServerResponse] =
    Ajaxer[SijAjaxApi].getLatestAndPreviousArticle(id).call()

  def getReducedViewOfLatestArticles: Future[ServerResponse] = Ajaxer[SijAjaxApi].getReducedViewOfLatestArticles().call()

  /** Gets the latest articles of a merchant or supplier */
  def getLatestArticlesOf(id: MerchantOrSupplierId): Future[ServerResponse] =
    Ajaxer[SijAjaxApi].getLatestArticlesOf(id).call()

  def saveNewArticle(formData: ArticleFormData): Future[ServerResponse] = Ajaxer[SijAjaxApi].saveNewArticle(formData).call()

  def saveNewVersionOfArticle(articleWithMetaInfo: ArticleWithMetaInfo): Future[ServerResponse] =
    Ajaxer[SijAjaxApi].saveNewVersionOfArticle(articleWithMetaInfo).call()

  def saveNewVersionOfArticles(articlesToSave: Seq[ReducedViewOfArticle]): Future[ServerResponse] =
    Ajaxer[SijAjaxApi].updateArticles(articlesToSave).call()

  def deleteArticle(articleId: ArticleId): Future[ServerResponse] = Ajaxer[SijAjaxApi].deleteArticle(articleId).call()

}

