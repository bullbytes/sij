package client.utils

import org.scalajs.dom.window
import shared.articleformdata.MerchantOrSupplierId
import shared.pages.PageInfo.SijPageInfo

/**
  * Client utility for navigating around the pages of this application.
  * <p>
  * Created by Matthias Braun on 11/2/2016.
  */
object Navigation {
  def changeTo(pagePath: String) = window.location.href = s"/$pagePath"

  /** Changes to a new `page` with the content of the given `merchantOrSupplier` */
  def changeTo(merchantOrSupplierId: MerchantOrSupplierId, page: SijPageInfo) = {
    // This replaces the part after "http://localhost"
    window.location.href = s"/${merchantOrSupplierId.id}/${page.path}"
  }

}
